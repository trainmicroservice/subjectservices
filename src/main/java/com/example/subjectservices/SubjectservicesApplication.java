package com.example.subjectservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubjectservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubjectservicesApplication.class, args);
	}
}
